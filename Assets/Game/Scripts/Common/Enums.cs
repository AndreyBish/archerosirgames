﻿namespace Common
{
	public static class Enums
	{
		public enum Axis
		{
			X,
			Y,
			Z
		}

		public enum ResourceType
		{
			Money,
		}
		
		public enum PlayerType
		{
			None = 0,
		}


		public enum BulletType
		{
			None = 0,
		}
		
		public enum WeaponType
		{
			None = 0,
		}

		
		public enum EnemyType
		{
			None = 0,
		}

		public enum AIState
		{
			GetResource,
			Waiting,
			Payment,
			Exit
		}


		public enum ParticleType
		{
			None       = 0,
		}

		public enum TeamType
		{
			None   = 0,
			Player = 1,
			Enemy  = 2,
		}
	}
}